package com.gtuto.prueba;

public class Numero {
	
	int num;
	
	public Numero(int num) {
		this.num = num;
	}
	
	public int sumaNum() {
		
		int suma = 0;
		int residuo;
		
		while(num >0) {
			residuo = num %10;
			suma = suma + residuo;
			num = num /10;
		}
		
		return suma;
	}
}
